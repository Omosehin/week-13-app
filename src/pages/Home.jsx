import React,{ Component } from 'react';
import './Home.css';
import img5 from '../images/Avatar.jpg';
import img6 from '../images/Black-panther.jpg';
import img8 from '../images/jurrasic.jpg';
// import img16 from '../images/black panther3.jpg';
import img25 from '../images/infinty.jpg';
// import img26 from '../images/spider_man_in_avengers_infinity_war.jpg';
import img27 from '../images/Race.jpg';
// import img28 from '../images/Raid.jpg';
import img29 from '../images/venom.jpg';
// import img30 from '../images/The-Maze-Runner-The-Death-Cure.jpg';
// import img31 from '../images/Avatar.jpg';
// import img32 from '../images/black-panther-3463377_640.jpg';
// import img3 from '../images/The-Maze-Runner-The-Death-Cure.jpg';
// import img34 from '../images/download.jpg';
// import img35 from '../images/p05x8xyq.jpg';
// import img36 from '../images/jurrasic.jpg';
// import img37 from '../images/pexels-photo.jpg';
// import img38 from '../images/kk.jpg';
// import img39 from '../images/black panther3.jpg';
class Home extends Component{
    render(){
        return(
            <div className= "container">				
				<div id="Video" className="container">
						<div className="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
								<h1 className="display-4">Video</h1>
								<p className="lead">Watch the latest trending Movie</p>	
		
						</div>	
						<div className="row">
								<div className="col-lg-4">
								<img src={img6} alt="Panther" width="350" height="500"/>
									{/* <img src="/images/Black-panther.jpg" width="350" height="500" /> */}
									<h2>Black-panther</h2>
									<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
										 Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
								</div>
								<div className="col-lg-4">
								<img src={img5} alt="Avatar" width="350" height="500"/>
										<h2>Sparroic</h2>
										<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
											 Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
								</div>
								<div className="col-lg-4">
								<img src={img27} alt="Race" width="350" height="500"/>										<h2>Race</h2>
										<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
											 Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
								</div>
								
								<div className="col-lg-4 image2">
								<img src={img8} alt="Jurassic" width="350" height="500"/>									<h2>Maze</h2>
									<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
										 Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
								</div>
								<div className="col-lg-4 image2">
								<img src={img25} alt="Infinty" width="350" height="500"/>										<h2>Heading</h2>
										<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
											 Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
								</div>
								<div className="col-lg-4 image2">
								<img src={img29} alt="Venom" width="350" height="500"/>										<h2>Warlock</h2>
										<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. 
											Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
								</div>
								
      					</div>
												
				</div>																																				 					
            </div>
        );
    }
}
export default Home