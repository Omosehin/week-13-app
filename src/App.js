import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from'react-router-dom';
// import logo from './logo.svg';
import './App.css';
import Home from './pages/Home.jsx';
import Jumbotron from'./components/Jumbotron.jsx';
import Navbar from './components/Navbar';
import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
         <Route exact path="/" component ={Navbar}/>
          <Route exact path="/" component ={Jumbotron}/>
          <Route exact path ="/" component={Home}/>
          <Route exact path ="/" component={Footer}/>
        </div>
      </Router>
    );
  }
}

export default App;
