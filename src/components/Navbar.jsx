import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import logo from '../images/logo.png';
class Navbar extends Component {
    render() {
        return (
            <div className="container-fluid navbar-flow">
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            
            <Link className="navbar-brand" to="/"><img src={logo} alt = "Logo" className="logo" /><strong>Daydawn Rental</strong></Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            
            <div className="collapse navbar-collapse" id="navbarsExample05">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/gallery">Gallery</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/contact">Contact Us</Link>
                </li>
                <ul className="nav navbar-nav navbar-left ml-auto mr-auto">
                          <li className="nav-item "><Link className="nav-link " to="/">Register</Link></li>
                </ul>
              </ul>
               
              <form className="form-inline my-2 my-md-0">
                <input className="form-control" type="text" placeholder="Search" />&nbsp;
              <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>
            </div>
            
          </nav>
          </div>
		
        );
    }
}

export default Navbar