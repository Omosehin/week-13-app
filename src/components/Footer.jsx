import React, { Component } from 'react';
import './Footer.css';
import img33 from '../images/up-arrow-inside-circle.png';

class Footer extends Component {
    render() {
        return (
           
                <div className="container">
                                <footer className="pt-4 my-md-5 pt-md-5">
                                        {/* <p className="float-right"><Link href="#"><img src="{img33}"/> </Link> </p> */}
                                    <div className="row">
                                        <div className="col-12 col-md">
                                         <img className="mb-2" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24" />
                                            <small className="d-block mb-3 text-muted">&copy; 2017-2018</small>
                                        </div>
                                        <div className="col-6 col-md">
                                            <h5>Features</h5>
                                            <ul className="list-unstyled text-small">
                                                <li className="text-muted" href="#">Cool stuff</li>
                                                <li className="text-muted" href="#">Random feature</li>	
                                            </ul>	
                                        </div>
                                        <div className="col-6 col-md">
                                            <h5>Resources</h5>
                                            <ul className="list-unstyled text-small">
                                                <li className="text-muted" href="#">Resource></li>
                                                <li className="text-muted" href="#">Resource name></li>
                                                
                                            </ul>
                                        </div>
                                        <div className="col-6 col-md">
                                            <h5>About</h5>
                                            <ul className="list-unstyled text-small">
                                                <li className="text-muted" href="#">Team</li>
                                                <li className="text-muted" href="#">Locations</li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                </div>
                       
           
        );
    }
}

export default Footer