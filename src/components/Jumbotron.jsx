import React,{ Component } from 'react';
import './Jumbotron.css';

class Jumbotron extends Component {
    render() {
        return (
            <div className="jumbotron jumbotron-fluid jumbotron-top">
            <div className="container">
            <h1 className="display-3">{this.props.title}</h1>
            <p className="lead">{this.props.subtitle}</p>
          <p>Here we celebrate the most inspiring examples of the cinematic one-sheet, and ask some
              <br/> leading designers for their own take on why they work so well...</p>
            </div>
            </div>
        );
    }
}

export default Jumbotron